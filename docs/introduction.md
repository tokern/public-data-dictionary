---
id: introduction
title: Data Dictionary for Public Data Sets
sidebar_label: Introduction
slug: /
---

This website contains information about popular public data sets. The data sets are 
organized by 

* hosting organization
* schema
* table

## Tech Stack

The data dictionary was generated with:
* [DbDocs](https://github.com/tokern/dbdocs)
* [DbCat](https://github.com/tokern/dbcat)
* [Docusaurus](https://v2.docusaurus.io)

It is kept up to date using Github Actions.

It is hosted on [Vercel](https://vercel.com/)

## Roll your own Data Dictionary

Interested in running your own data dictionary and automating database documentation ?
Learn more about [Tokern Data Dictionary](https://tokern.io/data-dictionary/)
