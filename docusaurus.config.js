module.exports = {
  title: 'Tokern Data Dictionary',
  tagline: 'Tokern Data Dictionary is an easy to use data dictionary for your data lake',
  url: 'https://tokern.io/data-dictionary/',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'tokern', // Usually your GitHub org/user name.
  projectName: 'dbdocs', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Tokern',
      hideOnScroll: true,
      logo: {
        alt: 'Tokern Logo',
        src: 'img/favicon.png',
        href: 'https://tokern.io/',
        target: '_self',
      },
      items: [
        {
          to: '/',
          activeBasePath: '/',
          label: 'Docs',
          position: 'left',
        },
        {href: 'https://tokern.io/data-dictionary/', target: '_self', label: 'Project', position: 'left'},
        {
          href: 'https://github.com/tokern/dbcat/',
          label: 'GitHub',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Data Dictionary',
              to: 'https://tokern.io/docs/data-dictionary/',
            },
            {
              label: 'Data Lineage',
              to: 'https://tokern.io/docs/data-lineage/',
            },
            {
              label: 'PIICatcher',
              to: 'https://tokern.io/docs/piicatcher/',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Data Dictionary',
              to: 'https://tokern.io/data-dictionary/',
            },
            {
              label: 'DbCat',
              href: 'https://github.com/tokern/dbcat',
            },
            {
              label: 'DbDocs',
              href: 'https://github.com/tokern/dbdocs',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Twitter',
              href: 'https://twitter.com/tokernhq',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Tokern, Inc. Built with Docusaurus.`,
    },
    gtag: {
      trackingID: process.env.GOOGLE_ANALYTICS_KEY,
      // Optional fields.
      anonymizeIP: true, // Should IPs be anonymized?
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          routeBasePath: '/',
          // Please change this to your repo.
          editUrl:
            'https://github.com/tokern/dbdocs/edit/main/website/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
