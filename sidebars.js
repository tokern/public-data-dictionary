// ** Sidebar managed by DbCat. DO NOT EDIT ** //
module.exports = {
  someSidebar: {
    'Data Dictionary': [
        'introduction',
        {
          "BigQuery Public DataSets": [
              {
                  "libraries_io": [
                    "BigQuery Public DataSets/libraries_io/dependencies",
                    "BigQuery Public DataSets/libraries_io/tags",
                    "BigQuery Public DataSets/libraries_io/versions",
                    "BigQuery Public DataSets/libraries_io/repository_dependencies",
                    "BigQuery Public DataSets/libraries_io/projects_with_repository_fields",
                    "BigQuery Public DataSets/libraries_io/repositories",
                    "BigQuery Public DataSets/libraries_io/projects",
                    
                 ]
                },
             {
                  "covid19_jhu_csse": [
                    "BigQuery Public DataSets/covid19_jhu_csse/recovered_cases",
                    "BigQuery Public DataSets/covid19_jhu_csse/summary",
                    "BigQuery Public DataSets/covid19_jhu_csse/deaths",
                    "BigQuery Public DataSets/covid19_jhu_csse/confirmed_cases",
                    
                 ]
                },
             
           ],
         
        }
    ],
  },
};